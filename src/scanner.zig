const std = @import("std");

const dprint = std.debug.print;

pub const TokenType = enum {
    // single-character tokens.
    token_left_paren,
    token_right_paren,
    token_left_brace,
    token_right_brace,
    token_comma,
    token_dot,
    token_minus,
    token_plus,
    token_semicolon,
    token_slash,
    token_star,

    // one or two character tokens.
    token_bang,
    token_bang_equal,
    token_equal,
    token_equal_equal,
    token_greater,
    token_greater_equal,
    token_less,
    token_less_equal,

    // literals.
    token_identifier,
    token_string,
    token_number,

    // keywords.
    token_and,
    token_class,
    token_else,
    token_false,
    token_for,
    token_fun,
    token_if,
    token_nil,
    token_or,
    token_print,
    token_return,
    token_super,
    token_this,
    token_true,
    token_var,
    token_while,

    token_error,
    token_eof,
};

pub const Token = struct {
    tp: TokenType,
    lexeme: []const u8,
    line: usize,
    char: usize,
};

fn isDigit(c: u8) bool {
    return c >= '0' and c <= '9';
}

fn isAlpha(c: u8) bool {
    return (c >= 'a' and c <= 'z') or
        (c >= 'A' and c <= 'Z') or
        c == '_';
}

fn isWhitespace(c: u8) bool {
    return c == ' ' or c == '\n' or c == '\t';
}

pub const Scanner = struct {
    source: []const u8,
    start: usize,
    current: usize,
    line: usize,
    char: usize,

    pub fn scanToken(self: *Scanner) Token {
        self.skipWhitespace();
        if (self.isAtEnd()) {
            return self.makeToken(TokenType.token_eof);
        }
        self.start = self.current;

        const c = self.advance();

        if (isAlpha(c)) {
            return self.identifier();
        }
        if (isDigit(c)) {
            return self.number();
        }

        switch (c) {
            // Single-character tokens
            '(' => return self.makeToken(TokenType.token_left_paren),
            ')' => return self.makeToken(TokenType.token_right_paren),
            '{' => return self.makeToken(TokenType.token_left_brace),
            '}' => return self.makeToken(TokenType.token_right_brace),
            ';' => return self.makeToken(TokenType.token_semicolon),
            ',' => return self.makeToken(TokenType.token_comma),
            '.' => return self.makeToken(TokenType.token_dot),
            '-' => return self.makeToken(TokenType.token_minus),
            '+' => return self.makeToken(TokenType.token_plus),
            '/' => return self.makeToken(TokenType.token_slash),
            '*' => return self.makeToken(TokenType.token_star),

            // One or two character tokens
            '!' => {
                return self.makeToken(if (self.match('=')) TokenType.token_bang_equal else TokenType.token_bang);
            },
            '=' => {
                return self.makeToken(if (self.match('=')) TokenType.token_equal_equal else TokenType.token_equal);
            },
            '<' => {
                return self.makeToken(if (self.match('=')) TokenType.token_less_equal else TokenType.token_less);
            },
            '>' => {
                return self.makeToken(if (self.match('=')) TokenType.token_greater_equal else TokenType.token_greater);
            },
            '"' => {
                return self.string();
            },
            else => return self.errorToken("unexpected character."),
        }
    }

    fn checkKeyword(self: *Scanner, lexeme: []const u8, tp: TokenType) TokenType {
        if (self.current - self.start == lexeme.len and std.mem.eql(u8, self.source[self.start .. self.start + lexeme.len], lexeme)) {
            return tp;
        }
        return TokenType.token_identifier;
    }

    fn identifierType(self: *Scanner) TokenType {
        switch (self.source[self.start]) {
            'a' => return self.checkKeyword("and", TokenType.token_star),
            'c' => return self.checkKeyword("class", TokenType.token_class),
            'e' => return self.checkKeyword("else", TokenType.token_else),
            'f' => {
                if (self.current - self.start > 1) {
                    switch (self.source[self.start + 1]) {
                        'a' => return self.checkKeyword("false", TokenType.token_false),
                        'o' => return self.checkKeyword("for", TokenType.token_for),
                        'u' => return self.checkKeyword("fun", TokenType.token_fun),
                        else => return TokenType.token_identifier,
                    }
                }
            },
            'i' => return self.checkKeyword("if", TokenType.token_if),
            'n' => return self.checkKeyword("nil", TokenType.token_nil),
            'o' => return self.checkKeyword("or", TokenType.token_or),
            'p' => return self.checkKeyword("print", TokenType.token_print),
            'r' => return self.checkKeyword("return", TokenType.token_return),
            's' => return self.checkKeyword("super", TokenType.token_super),
            't' => {
                if (self.current - self.start > 1) {
                    switch (self.source[self.start + 1]) {
                        'h' => return self.checkKeyword("this", TokenType.token_this),
                        'r' => return self.checkKeyword("true", TokenType.token_true),
                        else => return TokenType.token_identifier,
                    }
                }
            },
            'v' => return self.checkKeyword("var", TokenType.token_var),
            'w' => return self.checkKeyword("while", TokenType.token_while),
            else => return TokenType.token_identifier,
        }

        return TokenType.token_identifier; // this is ultimately unnecessary
    }

    fn identifier(self: *Scanner) Token {
        while (isAlpha(self.peek()) or isDigit(self.peek()))
            _ = self.advance();

        return self.makeToken(self.identifierType());
    }

    fn number(self: *Scanner) Token {
        while (isDigit(self.peek()))
            _ = self.advance();

        if (self.peek() == '.' and isDigit(self.peekNext())) {
            _ = self.advance();
            while (isDigit(self.peek()))
                _ = self.advance();
        }

        return self.makeToken(TokenType.token_number);
    }

    fn string(self: *Scanner) Token {
        while (self.peek() != '"' and !self.isAtEnd()) {
            if (self.peek() == '\n') {
                self.line += 1;
                self.char = 0;
            }
            _ = self.advance();
        }

        if (self.isAtEnd()) {
            return self.errorToken("Unterminated string.");
        }

        // the closing quote
        _ = self.advance();
        return self.makeToken(TokenType.token_string);
    }

    fn skipWhitespace(self: *Scanner) void {
        while (true) {
            if (self.isAtEnd())
                return;
            switch (self.peek()) {
                ' ', '\t', '\r' => _ = self.advance(),
                '\n' => {
                    _ = self.advance();
                    self.line += 1;
                    self.char = 0;
                },
                '/' => {
                    if (self.peekNext() == '/') {
                        while (self.peekNext() != '\n' and !self.isAtEnd())
                            _ = self.advance();
                    } else {
                        return;
                    }
                },
                else => return,
            }
        }
    }

    // NOTE: this will fail if I call it just before the end of the stream.
    fn peekNext(self: Scanner) u8 {
        if (self.isAtEnd())
            return 0;
        return self.source[self.current + 1];
    }

    fn peek(self: Scanner) u8 {
        return self.source[self.current];
    }

    fn match(self: *Scanner, expected: u8) bool {
        if (self.isAtEnd())
            return false;
        if (self.source[self.current] != expected)
            return false;

        self.current += 1;
        self.char += 1;
        return true;
    }

    fn advance(self: *Scanner) u8 {
        self.current += 1;
        self.char += 1;
        return self.source[self.current - 1];
    }

    fn isAtEnd(self: Scanner) bool {
        return self.current >= self.source.len;
    }

    fn makeToken(self: Scanner, tp: TokenType) Token {
        return Token{
            .tp = tp,
            // Ignore enclosing quotes if we are creating a string
            .lexeme = if (tp == TokenType.token_string) self.source[self.start + 1 .. self.current - 1] else self.source[self.start..self.current],
            .line = self.line,
            .char = self.char,
        };
    }

    fn errorToken(self: Scanner, msg: []const u8) Token {
        return Token{
            .tp = TokenType.token_error,
            .lexeme = msg,
            .line = self.line,
            .char = self.char,
        };
    }
};

pub fn initScanner(source: []const u8) Scanner {
    return Scanner{ .source = source, .start = 0, .current = 0, .line = 1, .char = 1 };
}
