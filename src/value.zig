// zlox -- An implementation of the Lox language in Zig
// Copyright (C) 2024 Panagiotis Koutsourakis <kutsurak@zarniwoop.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");
const object = @import("object.zig");
const vm = @import("vm.zig");
const chunk = @import("chunk.zig");

const Allocator = std.mem.Allocator;

const Obj = object.Obj;
const ObjData = object.ObjData;
const ObjString = object.ObjString;

const Chunk = chunk.Chunk;

const dprint = std.debug.print;

pub const Value = union(enum) {
    nil: void,
    boolean: bool,
    num: f32,
    obj: *Obj,

    pub fn negate(self: Value) Value {
        return numberVal(-self.num);
    }

    pub fn isNumber(self: Value) bool {
        return switch (self) {
            Value.num => true,
            else => false,
        };
    }

    pub fn isNil(self: Value) bool {
        return switch (self) {
            Value.nil => true,
            else => false,
        };
    }

    pub fn isBoolean(self: Value) bool {
        return switch (self) {
            Value.boolean => true,
            else => false,
        };
    }

    pub fn isObj(self: Value) bool {
        return switch (self) {
            Value.obj => true,
            else => false,
        };
    }

    pub fn isString(self: Value) bool {
        if (!self.isObj()) {
            return false;
        }

        switch (self.obj.*.data) {
            ObjData.str => {
                return true;
            },
            // else => return false,
        }
    }

    pub fn asNumber(self: Value) f32 {
        return self.num;
    }

    pub fn asBoolean(self: Value) bool {
        return self.boolean;
    }

    pub fn asObj(self: Value) *Obj {
        return self.obj;
    }

    pub fn asString(self: Value) []u8 {
        return self.obj.data.str.chars;
    }

    pub fn isFalsey(self: Value) bool {
        // NIL and FALSE are false, everything else is true
        return self.isNil() or (self.isBoolean() and !self.boolean);
    }

    pub fn isEqual(self: Value, other: Value) bool {
        switch (self) {
            Value.num => |n| {
                if (other.isNumber()) {
                    return n == other.num;
                } else {
                    return false;
                }
            },
            Value.boolean => |b| {
                if (other.isBoolean()) {
                    return b == other.boolean;
                } else {
                    return false;
                }
            },
            Value.nil => return other.isNil(),
            Value.obj => {
                if (self.isString() and other.isString()) {
                    return std.mem.eql(u8, self.obj.*.data.str.chars, other.obj.*.data.str.chars);
                } else {
                    return false;
                }
            },
        }
    }
};

fn printObject(value: Value) void {
    switch (value.obj.*.data) {
        ObjData.str => |str| dprint("\"{s}\"", .{str.chars}),
    }
}

pub fn printValue(v: Value) void {
    switch (v) {
        Value.num => |n| dprint("{d:.4}", .{n}),
        Value.boolean => |b| dprint("{any:>4}", .{b}),
        Value.obj => printObject(v),
        Value.nil => dprint("NIL", .{}),
    }
}

pub fn addNum(a: Value, b: Value) Value {
    return numberVal(a.num + b.num);
}

pub fn subNum(a: Value, b: Value) Value {
    return numberVal(a.num - b.num);
}

pub fn mulNum(a: Value, b: Value) Value {
    return numberVal(a.num * b.num);
}

pub fn divNum(a: Value, b: Value) Value {
    return numberVal(a.num / b.num);
}

pub fn greaterNum(a: Value, b: Value) Value {
    return booleanVal(a.num > b.num);
}

pub fn lessNum(a: Value, b: Value) Value {
    return booleanVal(a.num < b.num);
}

pub fn numberVal(n: f32) Value {
    return Value{ .num = n };
}

pub fn booleanVal(b: bool) Value {
    return Value{ .boolean = b };
}

pub fn objVal(obj: *Obj) Value {
    return Value{ .obj = obj };
}

pub fn stringVal(s: []const u8, c: *Chunk) Allocator.Error!Value {
    const obj = try c.allocator.create(Obj);

    // if (c.strings.getEntry(s)) |e| {
    //     obj.data.str = e.value_ptr.*;
    // } else {
    obj.data.str = try object.allocateString(s, c);
    // }

    // obj.data.str = objstr;
    return Value{ .obj = obj };
}

// pub fn objVal(comptime T: type, data: T) Value {
//     if (T == []u8) {
//         return Value{ .obj = &ObjString{ .chars = data } };
//     }
//     // return Value{ .obj = obj };
// }

pub fn nilVal() Value {
    return Value.nil;
}
