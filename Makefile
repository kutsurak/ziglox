SRCS = src/main.zig     \
	   src/chunk.zig    \
	   src/compiler.zig \
	   src/debug.zig    \
	   src/object.zig   \
	   src/root.zig     \
	   src/scanner.zig  \
	   src/value.zig    \
	   src/vm.zig       \
	   build.zig

.PHONY: all
all: build

build: $(SRCS)
	zig build

.PHONY: repl
repl: build
	zig build run

.PHONY: clean
clean:
	rm -rf zig-cache/ zig-out/
