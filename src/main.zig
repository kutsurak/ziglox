// zlox -- An implementation of the Lox language in Zig
// Copyright (C) 2024 Panagiotis Koutsourakis <kutsurak@zarniwoop.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");
const stdout = std.io.getStdOut().writer();
const stdin = std.io.getStdIn().reader();

const zlox = @import("zlox");

const OpCode = zlox.OpCode;
const Chunk = zlox.Chunk;
const Value = zlox.Value;
const VM = zlox.VM;
const InterpreterResult = zlox.InterpreterResult;
const InterpreterError = zlox.InterpreterError;

extern fn compile(source: *const []const u8, len: usize, chunk: *Chunk) bool;

const dprint = std.debug.print;

fn interpret(source: []const u8, alloc: std.mem.Allocator) zlox.InterpreterError!InterpreterResult {
    // TODO: Maybe initialize the chunk in the compiler?
    var chunk = try Chunk.init(alloc);
    defer chunk.deinit();

    if (!compile(&source, source.len, &chunk)) {
        return InterpreterError.CompileError;
    }

    var vm = VM.init(&chunk, alloc);
    defer vm.deinit();

    return vm.interpret();
}

fn repl(alloc: std.mem.Allocator) !void {
    const BUFLEN = 1024;
    var line: std.BoundedArray(u8, BUFLEN) = .{};
    var cntWriter = std.io.countingWriter(line.writer());

    try stdout.print("zlox> ", .{});
    while (stdin.streamUntilDelimiter(cntWriter.writer(), '\n', null)) {
        try cntWriter.writer().writeByte('\n');
        dprint("bytes written: {d}\n", .{cntWriter.bytes_written});
        _ = interpret(line.buffer[0..cntWriter.bytes_written], alloc) catch {
            // ignores errors
            // continue;
        };

        // empty the line
        try line.resize(0);
        try line.ensureUnusedCapacity(line.capacity());
        cntWriter = std.io.countingWriter(line.writer());
        try stdout.print("zlox> ", .{});
    } else |err| switch (err) {
        error.EndOfStream => {
            try stdout.print("\n", .{});
        },
        else => return err,
    }
}

fn readFile(fname: []const u8, alloc: std.mem.Allocator) ![]u8 {
    var fl = try std.fs.openFileAbsolute(fname, .{});
    defer fl.close();

    // read up to 1 MB
    return fl.readToEndAlloc(alloc, 100000);
}

fn runFile(fname: []const u8, alloc: std.mem.Allocator) !void {
    const source = try readFile(fname, alloc);
    _ = interpret(source, alloc) catch |err| {
        switch (err) {
            InterpreterError.CompileError => {
                std.process.exit(65);
            },
            InterpreterError.InternalError, InterpreterError.EmptyStack, InterpreterError.ValueTypeError, InterpreterError.OutOfMemory => {
                std.process.exit(70);
            },
        }
    };
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{ .safety = true }){};
    defer _ = gpa.deinit();
    var arena = std.heap.ArenaAllocator.init(gpa.allocator());
    defer arena.deinit();

    if (std.os.argv.len == 1) { // no arguments
        try repl(arena.allocator());
    } else {
        try runFile(std.mem.span(std.os.argv[1]), arena.allocator());
    }
}
