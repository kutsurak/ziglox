// zlox -- An implementation of the Lox language in Zig
// Copyright (C) 2024 Panagiotis Koutsourakis <kutsurak@zarniwoop.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");
const value = @import("value.zig");
const object = @import("object.zig");

const math = std.math;
const Allocator = std.mem.Allocator;
const AutoArrayHashMap = std.AutoArrayHashMap;

pub const Value = value.Value;
pub const numberVal = value.numberVal;
pub const boolVal = value.boolVal;
pub const stringVal = value.stringVal;
pub const ObjString = object.ObjString;

/// The list of Op Codes
pub const OpCode = enum {
    constant,
    longConstant,
    litNil,
    litTrue,
    litFalse,
    equal,
    greater,
    less,
    negate,
    add,
    sub,
    mul,
    div,
    logNot,
    ret,
};

const LineInfo = struct {
    repeat: usize,
    data: usize,

    pub fn new(data: usize) LineInfo {
        return LineInfo{ .repeat = 1, .data = data };
    }

    pub fn inc(li: *LineInfo) void {
        li.repeat += 1;
    }
};

/// A Chunk is a series of bytecode instructions.  We use an ArrayList(OpCode) to represent it.
pub const Chunk = struct {
    allocator: Allocator,
    code: std.ArrayList(u8),
    lines: std.ArrayList(LineInfo),
    constants: std.ArrayList(Value),
    // strings: AutoArrayHashMap([]const u8, ObjString),

    pub fn init(allocator: std.mem.Allocator) Allocator.Error!Chunk {
        var ret = Chunk{
            .allocator = allocator,
            .code = std.ArrayList(u8).init(allocator),
            .lines = std.ArrayList(LineInfo).init(allocator),
            .constants = std.ArrayList(Value).init(allocator),
            // .strings = AutoArrayHashMap([]const u8, ObjString).init(allocator),
        };

        // Initialize very small constants
        try ret.constants.append(numberVal(0.0));
        try ret.constants.append(numberVal(1.0));

        return ret;
    }

    pub fn deinit(self: Chunk) void {
        self.constants.deinit();
        self.lines.deinit();
        self.code.deinit();
    }

    fn addLineInfo(self: *Chunk, line: usize) Allocator.Error!void {
        var ln = self.lines.popOrNull();
        if (ln == null) {
            try self.lines.append(LineInfo.new(line));
        } else if (ln.?.data != line) {
            try self.lines.append(ln.?);
            try self.lines.append(LineInfo.new(line));
        } else {
            ln.?.inc();
            try self.lines.append(ln.?);
        }
    }

    pub fn writeOpCode(self: *Chunk, op: OpCode, line: usize) Allocator.Error!void {
        try self.code.append(@intFromEnum(op));
        try self.addLineInfo(line);
    }

    pub fn writeByte(self: *Chunk, byte: u8, line: usize) Allocator.Error!void {
        try self.code.append(byte);
        try self.addLineInfo(line);
    }

    // TODO:
    // pub fn writeInt(self: *Chunk, comptime T: type, v: T) Allocator.Error!void {
    //
    // }

    pub fn addConstant(self: *Chunk, val: Value) Allocator.Error!usize {
        try self.constants.append(val);
        return self.constants.items.len - 1;
    }

    pub fn writeConstant(self: *Chunk, val: Value, line: usize) Allocator.Error!void {
        var idx: usize = undefined;

        switch (val) {
            Value.num => {
                // First check if our constant is 0 or 1.  These values are already in the constants.
                if (math.approxEqAbs(f32, 0.0, val.num, math.floatEps(f32))) {
                    idx = 0;
                } else if (math.approxEqRel(f32, 1.0, val.num, math.sqrt(math.floatEps(f32)))) {
                    idx = 1;
                } else {
                    idx = try self.addConstant(val);
                }
            },
            Value.boolean, Value.nil, Value.obj => idx = try self.addConstant(val),
        }
        // If our index is just one byte we can use the short const instruction.
        if (idx <= 0xff) {
            try self.writeOpCode(OpCode.constant, line);
            try self.writeByte(@truncate(idx), line); // @truncate is safe here because we know idx is just one byte.
        } else if (idx <= 0xffffff) { // We need the long const instruction.
            try self.writeOpCode(OpCode.longConstant, line);
            // We write the 3 rightmost bytes of idx in the order we find them.
            var mask: usize = 0xff0000;

            var bt: u8 = @truncate((idx & mask) >> 2 * 8);
            try self.writeByte(bt, line);

            mask >>= 8;
            bt = @truncate((idx & mask) >> 8);
            try self.writeByte(bt, line);

            mask >>= 8;
            bt = @truncate(idx & mask);
            try self.writeByte(bt, line);
        } else {
            // TODO: the index is way too long for us to handle.
        }
    }

    pub fn count(self: *const Chunk) usize {
        return self.code.items.len;
    }

    pub fn decodeLongConstantIndex(self: *const Chunk, offset: usize) usize {
        var ret: usize = 0;
        for (0..3) |i| {
            ret <<= 8;
            ret += self.code.items[offset + i];
        }

        return ret;
    }

    pub fn getLine(self: *const Chunk, offset: usize) usize {
        var sum: usize = 0;
        const ln = for (self.lines.items) |li| {
            sum += li.repeat;
            if (sum >= offset + 1) {
                break li.data;
            }
        } else 0;

        return ln;
    }
};

test "writeConstant test" {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    var ch = try Chunk.init(arena.allocator());
    defer ch.deinit();

    const val = Value{ .num = 8 };
    for (0..256) |_| {
        try ch.writeConstant(val, 42);
    }

    var offset: usize = 0;
    for (0..256) |i| {
        if (i < 254) {
            try std.testing.expectEqual(@intFromEnum(OpCode.constant), ch.code.items[offset]);
            try std.testing.expectEqual(i + 2, ch.code.items[offset + 1]);
            offset += 2;
        } else {
            try std.testing.expectEqual(@intFromEnum(OpCode.longConstant), ch.code.items[offset]);
            try std.testing.expectEqual(i + 2, ch.decodeLongConstantIndex(offset + 1));
            offset += 4;
        }
    }
}
