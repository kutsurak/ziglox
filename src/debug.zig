// zlox -- An implementation of the Lox language in Zig
// Copyright (C) 2024 Panagiotis Koutsourakis <kutsurak@zarniwoop.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");
const dprint = std.debug.print;

const chunk_mod = @import("chunk.zig");
const value_mod = @import("value.zig");
const Chunk = chunk_mod.Chunk;
const OpCode = chunk_mod.OpCode;
const Value = value_mod.Value;
const asNumber = value_mod.asNumber;
pub const printValue = value_mod.printValue;

fn simpleInstruction(name: []const u8, offset: usize) usize {
    dprint("{s}\n", .{name});
    return offset + 1;
}

fn constantInstruction(name: []const u8, chunk: *const Chunk, offset: usize) usize {
    const c: u8 = chunk.code.items[offset + 1];
    dprint("{s:<16} {d:>4} '", .{ name, c });
    printValue(chunk.constants.items[c]);
    dprint("'\n", .{});

    return offset + 2;
}

fn longConstantInstruction(name: []const u8, chunk: *const Chunk, offset: usize) usize {
    const c: usize = chunk.decodeLongConstantIndex(offset + 1);
    dprint("{s:<16} {d:>4} '", .{ name, c });
    printValue(chunk.constants.items[c]);
    dprint("'\n", .{});

    return offset + 4;
}

pub fn disassembleInstruction(chunk: *const Chunk, offset: usize) usize {
    dprint("{d:0>4} ", .{offset});
    const ln = chunk.getLine(offset);
    if (offset > 0 and chunk.getLine(offset - 1) == ln) {
        dprint("   | ", .{});
    } else {
        dprint("{d:>4} ", .{ln});
    }

    const instruction: OpCode = @enumFromInt(chunk.code.items[offset]);
    switch (instruction) {
        OpCode.constant => {
            return constantInstruction("constant", chunk, offset);
        },
        OpCode.longConstant => {
            return longConstantInstruction("longConstant", chunk, offset);
        },
        OpCode.litNil => {
            return simpleInstruction("litNil", offset);
        },
        OpCode.litFalse => {
            return simpleInstruction("litFalse", offset);
        },
        OpCode.litTrue => {
            return simpleInstruction("litTrue", offset);
        },
        OpCode.equal => {
            return simpleInstruction("equal", offset);
        },
        OpCode.greater => {
            return simpleInstruction("greater", offset);
        },
        OpCode.less => {
            return simpleInstruction("less", offset);
        },
        OpCode.negate => {
            return simpleInstruction("negate", offset);
        },
        OpCode.add => {
            return simpleInstruction("add", offset);
        },
        OpCode.sub => {
            return simpleInstruction("sub", offset);
        },
        OpCode.mul => {
            return simpleInstruction("mul", offset);
        },
        OpCode.div => {
            return simpleInstruction("div", offset);
        },
        OpCode.logNot => {
            return simpleInstruction("logNot", offset);
        },
        OpCode.ret => {
            return simpleInstruction("ret", offset);
        },
        // _ => {
        //     print("Unknown opcode: {d}\n", instruction);
        // },
    }
}

pub fn disassembleChunk(chunk: *const Chunk, name: []const u8) void {
    dprint("== {s} ==\n", .{name});

    var offset: usize = 0;
    while (offset < chunk.count()) {
        offset = disassembleInstruction(chunk, offset);
    }
    dprint("\n", .{});
}
