// zlox -- An implementation of the Lox language in Zig
// Copyright (C) 2024 Panagiotis Koutsourakis <kutsurak@zarniwoop.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");
const scanner = @import("scanner.zig");
const debug = @import("debug.zig");

const Allocator = std.mem.Allocator;
const Chunk = @import("chunk.zig").Chunk;
const OpCode = @import("chunk.zig").OpCode;
const Value = @import("chunk.zig").Value;
const numberVal = @import("chunk.zig").numberVal;
const stringVal = @import("chunk.zig").stringVal;
const Token = @import("scanner.zig").Token;
const TokenType = @import("scanner.zig").TokenType;
const Scanner = @import("scanner.zig").Scanner;

const debug_print_code = true;

const dprint = std.debug.print;

const Precedence = enum {
    prec_none,
    prec_assignment, // =
    prec_or, // or
    prec_end, // and
    prec_equality, // ==, !=
    prec_comparison, // <, >, <=, >=
    prec_term, // +, -
    prec_factor, // *, /
    prec_unary, // -, !
    prec_call, // ., ()
    prec_primary,
};

const FnPtr = *const fn (self: *Parser) void;

const ParseRule = struct {
    prefix: ?FnPtr,
    infix: ?FnPtr,
    precedence: Precedence,

    pub fn init(prefix: ?FnPtr, infix: ?FnPtr, precedence: Precedence) ParseRule {
        return ParseRule{ .prefix = prefix, .infix = infix, .precedence = precedence };
    }
};

const Parser = struct {
    alloc: Allocator,
    compilingChunk: *Chunk,
    current: Token,
    previous: Token,
    scn: Scanner,
    hadError: bool,
    panicMode: bool,
    rules: std.AutoArrayHashMap(TokenType, ParseRule),

    pub fn init(source: []const u8, chunk: *Chunk, alloc: Allocator) Parser {
        var p = Parser{ .alloc = alloc, .compilingChunk = chunk, .current = undefined, .previous = undefined, .scn = scanner.initScanner(source), .hadError = false, .panicMode = false, .rules = std.AutoArrayHashMap(TokenType, ParseRule).init(alloc) };
        p.initRules() catch |err| {
            dprint("FIXME {}\n", .{err});
        };
        return p;
    }

    pub fn deinit(self: *Parser) void {
        self.rules.deinit();
    }

    fn initRules(self: *Parser) !void {
        try self.rules.put(TokenType.token_left_paren, ParseRule.init(&Parser.grouping, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_right_paren, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_left_brace, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_right_brace, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_comma, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_dot, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_minus, ParseRule.init(&Parser.unary, &Parser.binary, Precedence.prec_term));
        try self.rules.put(TokenType.token_plus, ParseRule.init(null, &Parser.binary, Precedence.prec_term));
        try self.rules.put(TokenType.token_semicolon, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_slash, ParseRule.init(null, &Parser.binary, Precedence.prec_factor));
        try self.rules.put(TokenType.token_star, ParseRule.init(null, &Parser.binary, Precedence.prec_factor));
        try self.rules.put(TokenType.token_bang, ParseRule.init(&Parser.unary, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_bang_equal, ParseRule.init(null, &Parser.binary, Precedence.prec_equality));
        try self.rules.put(TokenType.token_equal, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_equal_equal, ParseRule.init(null, &Parser.binary, Precedence.prec_equality));
        try self.rules.put(TokenType.token_greater, ParseRule.init(null, &Parser.binary, Precedence.prec_comparison));
        try self.rules.put(TokenType.token_greater_equal, ParseRule.init(null, &Parser.binary, Precedence.prec_comparison));
        try self.rules.put(TokenType.token_less, ParseRule.init(null, &Parser.binary, Precedence.prec_comparison));
        try self.rules.put(TokenType.token_less_equal, ParseRule.init(null, &Parser.binary, Precedence.prec_comparison));
        try self.rules.put(TokenType.token_identifier, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_string, ParseRule.init(&Parser.string, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_number, ParseRule.init(&Parser.number, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_and, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_class, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_else, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_false, ParseRule.init(&Parser.literal, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_for, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_fun, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_if, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_nil, ParseRule.init(&Parser.literal, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_or, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_print, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_return, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_super, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_this, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_true, ParseRule.init(&Parser.literal, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_var, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_while, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_error, ParseRule.init(null, null, Precedence.prec_none));
        try self.rules.put(TokenType.token_eof, ParseRule.init(null, null, Precedence.prec_none));
    }

    fn getRule(self: *Parser, opType: TokenType) ParseRule {
        return self.rules.get(opType).?;
    }

    pub fn advance(self: *Parser) void {
        self.previous = self.current;

        while (true) {
            self.current = self.scn.scanToken();

            if (self.current.tp != TokenType.token_error)
                break;

            self.errorAtCurrent(self.current.lexeme);
        }
    }

    pub fn consume(self: *Parser, tp: TokenType, msg: []const u8) void {
        if (self.current.tp == tp) {
            self.advance();
            return;
        }

        self.errorAtCurrent(msg);
    }

    pub fn endCompiler(self: *Parser) void {
        self.emitReturn();
        if (debug_print_code) { // TODO: conditional compilation!
            if (!self.hadError) {
                debug.disassembleChunk(self.compilingChunk, "code");
            }
        }
    }

    fn errorAtCurrent(self: *Parser, msg: []const u8) void {
        self.errorAt(self.current, msg);
    }

    fn errorM(self: *Parser, msg: []const u8) void {
        self.errorAt(self.previous, msg);
    }

    fn errorAt(self: *Parser, token: Token, msg: []const u8) void {
        // do not print errors in panic mode
        if (self.panicMode)
            return;
        self.panicMode = true;
        dprint("Error {d}:{d}", .{ token.line, token.char });

        if (token.tp == TokenType.token_eof) {
            dprint(" at end", .{});
        } else if (token.tp == TokenType.token_error) {
            // Nothing
        } else {
            dprint(" at '{s}'", .{token.lexeme});
        }

        dprint(": {s}\n", .{msg});

        self.hadError = true;
    }

    fn emitByte(self: *Parser, byte: u8) void {
        self.compilingChunk.writeByte(byte, self.previous.line);
    }

    // fn emitBytes(self: *Parser, byte1: u8, byte2: u8)
    fn emitInstruction(self: *Parser, op: OpCode, byte: u8) void {
        self.emitOpCode(op);
        self.emitByte(byte);
    }

    fn emitOpCode(self: *Parser, op: OpCode) void {
        self.compilingChunk.writeOpCode(op, self.previous.line) catch |e| {
            if (e == std.mem.Allocator.Error.OutOfMemory)
                self.errorAt(self.previous, "Allocation error");
        };
    }

    fn emitConstant(self: *Parser, val: Value) void {
        self.compilingChunk.writeConstant(val, self.previous.line) catch |e| {
            if (e == std.mem.Allocator.Error.OutOfMemory)
                self.errorAt(self.previous, "Allocation error");
        };
    }

    fn emitReturn(self: *Parser) void {
        self.emitOpCode(OpCode.ret);
    }

    fn number(self: *Parser) void {
        const n = std.fmt.parseFloat(f32, self.previous.lexeme) catch {
            self.errorM("Bad number\n");
            return;
        };
        const v = numberVal(n);
        self.emitConstant(v);
    }

    fn string(self: *Parser) void {
        const val = stringVal(self.previous.lexeme, self.compilingChunk) catch {
            self.errorAt(self.previous, "String value allocation failed.");
            return;
        };
        self.emitConstant(val);
    }

    fn grouping(self: *Parser) void {
        // the initial '(' has been consumed
        self.expression();
        self.consume(TokenType.token_right_paren, "Expect ')' after expression.");
    }

    fn unary(self: *Parser) void {
        const opType: TokenType = self.previous.tp;

        // Compile the operand
        self.parsePrecedence(Precedence.prec_unary);

        switch (opType) {
            TokenType.token_minus => self.emitOpCode(OpCode.negate),
            TokenType.token_bang => self.emitOpCode(OpCode.logNot),
            else => return,
        }
    }

    fn binary(self: *Parser) void {
        // Remember the operator we just consumed
        const opType = self.previous.tp;

        const rule = self.getRule(opType);
        self.parsePrecedence(@enumFromInt(@intFromEnum(rule.precedence) + 1));

        switch (opType) {
            TokenType.token_plus => self.emitOpCode(OpCode.add),
            TokenType.token_minus => self.emitOpCode(OpCode.sub),
            TokenType.token_star => self.emitOpCode(OpCode.mul),
            TokenType.token_slash => self.emitOpCode(OpCode.div),
            TokenType.token_bang_equal => {
                self.emitOpCode(OpCode.equal);
                self.emitOpCode(OpCode.logNot);
            },
            TokenType.token_equal_equal => {
                self.emitOpCode(OpCode.equal);
            },
            TokenType.token_greater => {
                self.emitOpCode(OpCode.greater);
            },
            TokenType.token_greater_equal => {
                self.emitOpCode(OpCode.less);
                self.emitOpCode(OpCode.logNot);
            },
            TokenType.token_less => {
                self.emitOpCode(OpCode.less);
            },
            TokenType.token_less_equal => {
                self.emitOpCode(OpCode.greater);
                self.emitOpCode(OpCode.logNot);
            },
            else => return,
        }
    }

    fn literal(self: *Parser) void {
        switch (self.previous.tp) {
            TokenType.token_nil => self.emitOpCode(OpCode.litNil),
            TokenType.token_false => self.emitOpCode(OpCode.litFalse),
            TokenType.token_true => self.emitOpCode(OpCode.litTrue),
            else => return,
        }
    }

    fn parsePrecedence(self: *Parser, plevel: Precedence) void {
        self.advance();
        const prefixRule = self.getRule(self.previous.tp).prefix orelse {
            self.errorM("Expect expression");
            return;
        };

        prefixRule(self);

        while (@intFromEnum(plevel) <= @intFromEnum(self.getRule(self.current.tp).precedence)) {
            self.advance();
            const infixRule = self.getRule(self.previous.tp).infix.?;
            infixRule(self);
        }
    }

    pub fn expression(self: *Parser) void {
        self.parsePrecedence(Precedence.prec_assignment);
    }
};

pub fn compile(source: []const u8, chunk: *Chunk) bool {
    var gpa = std.heap.GeneralPurposeAllocator(.{ .safety = true }){};
    defer _ = gpa.deinit();
    var arena = std.heap.ArenaAllocator.init(gpa.allocator());
    defer arena.deinit();

    var parser = Parser.init(source, chunk, arena.allocator());
    defer parser.deinit();

    parser.advance();
    parser.expression();
    parser.consume(TokenType.token_eof, "Expected end of expression.");
    parser.endCompiler();

    return !parser.hadError;
}
