// zlox -- An implementation of the Lox language in Zig
// Copyright (C) 2024 Panagiotis Koutsourakis <kutsurak@zarniwoop.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");

const dprint = std.debug.print;
const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;

const chunk = @import("chunk.zig");
const value = @import("value.zig");
const object = @import("object.zig");

const Chunk = chunk.Chunk;
const OpCode = chunk.OpCode;
const Value = value.Value;
const Obj = object.Obj;
const ObjString = object.ObjString;

const printValue = value.printValue;

const debug_mod = @import("debug.zig");

pub const InterpreterResult = enum { OK };

const dbg = true;

pub const CompileError = error{CompileError};

pub const RuntimeError = error{
    InternalError,
    EmptyStack,
    ValueTypeError,
};

pub const InterpreterError = Allocator.Error || CompileError || RuntimeError;

pub const VM = struct {
    alloc: Allocator,
    chunk: *Chunk, // const maybe?
    ip: usize,
    stack: ArrayList(Value),
    objects: ?*Obj,

    pub fn init(c: *Chunk, alloc: Allocator) VM {
        return VM{ .chunk = c, .ip = 0, .stack = ArrayList(Value).init(alloc), .alloc = alloc, .objects = null };
    }

    pub fn deinit(self: *VM) void {
        var obj = self.objects;
        while (obj) |o| {
            const next = o.next;
            self.chunk.allocator.destroy(o);
            obj = next;
        }

        self.stack.deinit();
        // self.strings.deinit();
    }

    // TODO: The next four functions are C macros in the book, and it makes sense to inline them
    // somehow.
    fn readByte(self: *VM) u8 {
        const b = self.chunk.code.items[self.ip];
        self.advanceIP(1);

        return b;
    }

    fn advanceIP(self: *VM, i: usize) void {
        self.ip += i;
    }

    fn readConstant(self: *VM) Value {
        const i = self.readByte();

        return self.chunk.constants.items[i];
    }

    fn readLongConstant(self: *VM) Value {
        const i = self.chunk.decodeLongConstantIndex(self.ip);
        self.advanceIP(3);

        return self.chunk.constants.items[i];
    }

    fn binOp(self: *VM, comptime op: fn (a: Value, b: Value) Value) InterpreterError!void {
        if (!(try self.stackPeek(0)).isNumber() or !(try self.stackPeek(1)).isNumber()) {
            self.runtimeError("Operands must be numbers.", .{});
            return InterpreterError.ValueTypeError;
        }
        const b = try self.pop();
        const a = try self.pop();

        try self.push(op(a, b));
    }

    fn concatenate(self: *VM) InterpreterError!void {
        const b = try self.pop();
        const a = try self.pop();
        const sl = [2][]u8{ a.asString(), b.asString() };
        const conc = try std.mem.concat(self.chunk.allocator, u8, &sl);
        // GC will de-allocate

        const obj = try object.allocateObj(self);
        // GC will de-allocate
        obj.data.str = object.takeString(conc);
        try self.push(Value{ .obj = obj });
    }

    fn stackPeek(self: *VM, idx: usize) InterpreterError!Value {
        if (self.stack.items.len == 0) {
            self.runtimeError("stackPeek: empty stack.", .{});
            return InterpreterError.EmptyStack;
        }

        if (idx < 0 or idx >= self.stack.items.len) {
            self.runtimeError("stackPeek: bad index: {d} ({d})", .{ idx, self.stack.items.len });
            return InterpreterError.InternalError;
        }

        return self.stack.items[self.stack.items.len - 1 - idx];
    }

    fn pop(self: *VM) InterpreterError!Value {
        if (self.stack.items.len == 0) {
            self.runtimeError("Empty stack.", .{});
            return InterpreterError.EmptyStack;
        }

        return self.stack.pop();
    }

    fn push(self: *VM, val: Value) InterpreterError!void {
        try self.stack.append(val);
    }

    fn run(self: *VM) InterpreterError!InterpreterResult {
        var instruction: u8 = undefined;

        if (dbg) {
            dprint("== Tracing Execution ==\n", .{});
        }

        while (true) {
            if (dbg) {
                dprint("      ", .{});
                for (self.stack.items) |it| {
                    dprint("[ ", .{});
                    printValue(it);
                    dprint(" ]", .{});
                }
                dprint("\n", .{});
                _ = debug_mod.disassembleInstruction(self.chunk, self.ip);
            }
            instruction = self.readByte();
            const op: OpCode = @enumFromInt(instruction);
            switch (op) {
                OpCode.constant => {
                    const val = self.readConstant();
                    try self.push(val);
                },
                OpCode.longConstant => {
                    const val = self.readLongConstant();
                    try self.push(val);
                },
                OpCode.litNil => {
                    try self.push(value.nilVal());
                },
                OpCode.litFalse => {
                    try self.push(value.booleanVal(false));
                },
                OpCode.litTrue => {
                    try self.push(value.booleanVal(true));
                },
                OpCode.equal => {
                    const v2 = try self.pop();
                    const v1 = try self.pop();

                    try self.push(value.booleanVal(v1.isEqual(v2)));
                },
                OpCode.greater => {
                    try self.binOp(value.greaterNum);
                },
                OpCode.less => {
                    try self.binOp(value.lessNum);
                },
                OpCode.negate => {
                    if (self.stack.items.len == 0) {
                        if (dbg) {
                            dprint("\n", .{});
                        }
                        return InterpreterError.EmptyStack;
                    }

                    switch (self.stack.getLast()) {
                        Value.num => self.stack.items[self.stack.items.len - 1].num *= -1,
                        else => {
                            self.runtimeError("Operand must be a number.", .{});
                            if (dbg) {
                                dprint("\n", .{});
                            }
                            return InterpreterError.ValueTypeError;
                        },
                    }
                },
                OpCode.add => {
                    if ((try self.stackPeek(0)).isString() and (try self.stackPeek(1)).isString()) {
                        try self.concatenate();
                    } else if ((try self.stackPeek(0)).isNumber() and (try self.stackPeek(1)).isNumber()) {
                        // QUESTION: Is this good zig style? Is there some other way to do this?
                        const b = (try self.pop()).asNumber();
                        const a = (try self.pop()).asNumber();
                        try self.push(value.numberVal(a + b));
                    } else {
                        self.runtimeError("Operands must be two numbers or two strings.", .{});
                        if (dbg) {
                            dprint("\n", .{});
                        }
                        return InterpreterError.ValueTypeError;
                    }
                },
                OpCode.sub => {
                    try self.binOp(value.subNum);
                },
                OpCode.mul => {
                    try self.binOp(value.mulNum);
                },
                OpCode.div => {
                    try self.binOp(value.divNum);
                },
                OpCode.logNot => {
                    const v = try self.pop();
                    try self.push(value.booleanVal(v.isFalsey()));
                },
                OpCode.ret => {
                    const v = try self.pop();
                    printValue(v);
                    dprint("\n", .{});
                    if (dbg) {
                        dprint("\n", .{});
                    }
                    return InterpreterResult.OK;
                },
                // else => {
                //     return InterpreterError.InterpreterError;
                // },
            }
        }

        if (dbg) {
            dprint("\n", .{});
        }
        return InterpreterResult.OK;
    }

    pub fn interpret(self: *VM) InterpreterError!InterpreterResult {
        self.ip = 0;
        return self.run();
    }

    fn resetStack(self: *VM) void {
        self.stack.clearRetainingCapacity();
    }

    fn runtimeError(self: *VM, comptime msg: []const u8, args: anytype) void {
        dprint("Runtime error in line {d}:\n", .{self.chunk.getLine(self.ip - 1)});
        dprint(msg, args);
        dprint("\n", .{});

        self.resetStack();
    }
};
