const std = @import("std");
const testing = std.testing;
const config = @import("config");

const chunk_mod = @import("chunk.zig");
const debug_mod = @import("debug.zig");
const value_mod = @import("value.zig");
const vm_mod = @import("vm.zig");
const compiler_mod = @import("compiler.zig");

pub const OpCode = chunk_mod.OpCode;
pub const Chunk = chunk_mod.Chunk;
pub const Value = value_mod.Value;
pub const VM = vm_mod.VM;
pub const InterpreterResult = vm_mod.InterpreterResult;
pub const InterpreterError = vm_mod.InterpreterError;

const dprint = std.debug.print;

export fn compile(source: *const []const u8, len: usize, chunk: *Chunk) bool {
    return compiler_mod.compile(source.*[0..len], chunk);
}
