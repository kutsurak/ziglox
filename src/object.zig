// zlox -- An implementation of the Lox language in Zig
// Copyright (C) 2024 Panagiotis Koutsourakis <kutsurak@zarniwoop.org>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// We avoid the acrobatics C forces upon us with a tagged union.
const std = @import("std");
const value = @import("value.zig");
const vm = @import("vm.zig");
const chunk = @import("chunk.zig");

const Allocator = std.mem.Allocator;
const Chunk = chunk.Chunk;
const VM = vm.VM;

pub const ObjData = union(enum) {
    str: ObjString,
};

pub const Obj = struct {
    data: ObjData,
    next: ?*Obj,
};

pub const ObjString = struct {
    chars: []u8,
};

pub fn takeString(s: []u8) ObjString {
    return ObjString{ .chars = s };
}

pub fn copyString(s: []const u8, alloc: Allocator) Allocator.Error!ObjString {
    // Copy the incomming string, and create a new Object value from it.
    const ptr = try alloc.alloc(u8, s.len);
    std.mem.copyForwards(u8, ptr, s);

    return ObjString{ .chars = ptr };
}

pub fn allocateObj(v: *VM) Allocator.Error!*Obj {
    var o = try v.chunk.allocator.create(Obj);
    o.next = v.objects;
    v.objects = o;
    return o;
}

pub fn allocateString(s: []const u8, c: *Chunk) Allocator.Error!ObjString {
    const objstr = try copyString(s, c.allocator);
    // try c.strings.put(s, objstr);

    return objstr;
}
